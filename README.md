# OpenTech(AUC) website

Codebase for the [OpenTech(AUC) website](https://opentech_auc.codeberg.page).

Please [read the relevant
documentation](https://codeberg.org/opentech_auc/website/wiki).
