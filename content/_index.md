---
title: OpenTech(AUC)
description: OpenTech(AUC) main page
---

![AUC ASCII Art](/images/hero-banner.png)

We care about **Open Standards**, **Free Software**, and **Open Data**. We think
universities, and especially AUC, should be a place to learn and explore how
technology shapes us individually and as a society. Our group is composed
(primarily) of [students and
staff](https://codeberg.org/org/opentech_auc/members) at [Amsterdam University
College](https://www.auc.nl/). 

---

## Where do I start?

Here's [a few ways to get rid of big tech]({{< ref "/resources" >}}) in your
life.

---
## Curious? Let's chat.

We meet **Wednesdays at 15:30 in the AUC common room** ([Science Park 113, 1098
XG, Amsterdam](https://osm.org/go/0E6VM3NT0?node=8364364180)). If your calendar
supports it, you can keep it in sync with our events by [clicking here]({{< ref
"/events" "calendar" >}}) or pasting

```{{< ref "/events" "calendar" >}}```

in the app. Can't make it to the meetings? Join our
[Matrix](https://matrix.to/#/#opentech-auc:pub.solar) space, or reach out via
the [Fediverse](https://social.edu.nl/@OpenTech_AUC)!
