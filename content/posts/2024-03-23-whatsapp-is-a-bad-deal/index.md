---
title: WhatsApp is a bad deal
author: velthuis_misha
date: 2024-03-23
fediverse: 113597562379172498
imageAltAttribute: Disfigured Whatsapp logo
tags:
   - Open source
   - Free software
   - Politics
   - Economics
---

Normally, when you feel you're getting the short end of a stick, for example when the supermarket sells you expensive mushy cauliflowers, or when the organization you volunteer at treats you disrespectfully, you go and find what you need somewhere else.

This ability to "defect" to an alternative when you feel you're not getting a good deal, is an important "power" that helps you get reasonable deals.[^1]

When it comes to the way we relate to each other digitally, arguably one of the main ways in which we relate these days, we do not have that luxury. Apps like WhatsApp have grown to a point at which the [network effects](https://en.wikipedia.org/wiki/Network_effect) make it almost impossible for alternative suppliers to offer something similar for "consumers" to defect to.

As a result we are getting a bad deal.

## What does this bad deal look like?

There is economics (summarized as: it's simply a bad deal) and there is politics (summarized as: it's a bad deal with the wrong people).

### Economics

Starting with the economics: even if you feel that Whatsapp is "quite nice", the _deal_ that you're making is still bad when you're simply paying too much.

So what are you paying? You're paying with data. Whatsapp is encrypted, which means that the people from Meta/Facebook claim that they cannot see the content of your messages. What they _can_ see is the meta data (no pun intended): the size of the message, to whom it is sent, at what time, etc.. Connecting such information with the other information that they have (for example from cookies, Instagram, Threads, Facebook, etc.), gives them an extraordinary wealth of information about our (social) lives.

An innocent example: a friend searches for wedding cakes, and then starts sending a particular message of exactly the same size (in terms of bytes) to several contacts. The algorithms do not need to know the actual content of the message to know what kind of message is likely being sent, and they start showing adds for tuxedos and dresses to the other top 200 contacts, including those who had not even received the message yet, but may have only been talking about it ("craaaaazy, my phone must be listening to what we were talking about!!!!")[^2]. Now imagine a less innocent example during election times.

In the end, you don't even have to go into so much detail to see the problem. You just have to look at the profits of big tech to see that we're being screwed over: we might not be paying with money, but they sure find a way to turn whatever we're paying them with _into money_.[^3] And looking at the profits, that is apparently a lot more than what it costs to provide us with a shiny app.

In a "normal market" such high margins would draw in investors and competitors who would then offer a similar product at a slightly lower price, in their pursuit of a piece of the margin bonanza. But when it comes to a platform like Whatsapp, given its network effects, that kite does not fly.

![whatsap-creep](/images/threads.jpeg)

### Politics

Not all bad deals are equally bad. Paying a bit too much to someone who could really use the money is fine. The right thing to do even. But the opposite also holds: paying too much to people who already have a criminal amount of money, power and wealth is, well, criminal.

In all of this, it is important to remember that power (as in, the ability to control others) is a "zero sum" thing. Something like food is a positive sum thing: there is simply more food in the world than 200 years ago. But the amount of power in the world hasn't changed; it just gets redistributed. Sometimes it is shared among a few, sometimes it is shared among many. Sometimes it's held here, sometimes it's held there.

The point is: even when you strike a deal that on some material level benefits both Meta (they get your data) and you (you get their shiny app), when Meta benefits more than you, over time, when it comes to power, you _relatively_ loose out. Your future self will be less free and independent. Your kids will be less free and independent. (Their news channels will be bought and their politicians will be corrupted).

Rather than just accepting the short end of the stick, we should be thinking about how we can improve our lives and the lives of our children.

## Better deals

The good news is that there are better deals.

[Signal](https://signal.org/download/) for example, is run by a non-profit foundation, rather than a powerful corporation. And the code that it uses is publicly available. Although there is still the risk of lock-in (the more people use it, the more attractive it becomes, and the harder it will be to leave) the fact that the platform is not out to make a profit means that the risk of /abuse/ of such lock-in is much smaller. As a result we are getting a better deal.

Another alternative that is slowly getting ready for wide usage, is the Matrix protocol, as implemented in for example the application "[Element](https://element.io/)". A major advantage of this alternative, is that there is no central point of control (one server that all users use to find and communicate with each other). Instead, much like different organizations can each have their own email server (which was quite common before all organizations started to outsource their servers to Microsoft), Matrix allows everyone to set up their own Matrix server, which can then be used to communicate with other Matrix servers. The goal behind this is to maximize users' freedom (read: ability to defect), and to prevent any one person/group of people from gaining control over a large number of others.

Better digital worlds are available. The "only" challenge is to get people to accept the little bit of initial comfort that comes with exploring new alternatives. This is a relatively easy way to push for our freedom and the freedom of those who come after us.[^4]

[^1]: The other being sticking to your original engagement and raising your voice. See for example Hirschman's "Exit, Voice, and Loyalty".

[^2]: Unless you are some important politician or journalist ([link](https://www.theguardian.com/technology/2021/sep/13/nso-group-iphones-apple-devices-hack-patch)), it is unlikely that your everyday conversations are being listened to. The recordings would drain your battery, sending the recordings to Meta's servers would show up in your data usage, and processing/analyzing these recordings would cost Meta enormous amounts of computing power. Those eerily "well-placed" ads that makes you think you are being listened to should instead be a warning: the algorithms can already infer so much about us from what we voluntarily give them.

[^3]: Compare for example the net margins of Ahold (holding company of Albert Heijn) ([link](https://www.macrotrends.net/stocks/charts/ADRNY/ahold/profit-margins)) and the net margins of Meta ([link](https://www.macrotrends.net/stocks/charts/META/meta-platforms/profit-margins)).

[^4]: See for example [this](/posts/2024-02-28-auc-and-whatsapp) initiative.
