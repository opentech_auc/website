---
title: So, What's the Deal with Surveillance Capitalism?
author: natasha_mr
date: 2025-01-31
fediverse: 113985673466297084
featured:
  url: panopticon_image.jpg
  alt: A 21st century panopticon called Facebook.
  caption: How Foucault's thought adapted to the digital revolution
  credit: Rumor Ex Mundis 
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
   - Surveillance Capitalism 
   - Privacy 
---

The [panopticon](https://www.brown.edu/Departments/Joukowsky_Institute/courses/13things/7121.html) - a prison system designed to monitor the maximum number of prisoners with the least amount of guards. Designed by an English philosopher and social theorist, Jeremy Bentham, it consists of a central tower where the guards are, surrounded by the prison cells arranged in a circular building. 

The layout of this tower is such that the guards are able to look into the prisoners' cells at all times, but the prisoners are unable to see whether the guards are looking at them or not. This results in a feeling of constant surveillance, which in turn eliminates the need to discipline prisoners with forceful authority, as they discipline themselves because of the internalised sense of omniscience. As Foucault put it, "the inmate must never know whether he is being looked at at any one moment; but he must be sure that he may always be so."

This panopticon is much more than an architectural design; it is a metaphor for how our society operates. And just as Foucault put it in his work ["Discipline and Punish: The Birth of the Prison"](https://monoskop.org/images/4/43/Foucault_Michel_Discipline_and_Punish_The_Birth_of_the_Prison_1977_1995.pdf) during the major changes of the 20th century, it is equally as relevant now under the information age. 

Applying the concept of the panopticon to social media, the algorithm acts as a panopticon for users, constantly monitoring metrics and behaviours to be later used for profit-driven goals. This idea of social media or other platforms spying on us is commonly referred to as  **"surveillance capitalism"**, a term coined by [Shoshana Zuboff](https://en.wikipedia.org/wiki/Shoshana_Zuboff). Enormous wealth and power are concentrated in unsettling new "behavioural futures markets," where predictions about our actions are traded, and the creation of goods and services is dominated by a new system focused on shaping our behaviour.

It is no coincidence that the ad you got on Instagram about flights to Spain popped up right after you talked about it with friends. It is also no coincidence that so many people, especially of a young age, are never-endlessly doomscrolling through TikTok. The algorithm *knows* users, and knows how to manipulate their behaviour to get their attention. And for them, *your* attention is *their* money. This is also especially true for creators. On social media, efficiency and profitability are prioritised over artistic integrity. This resonates with Marx's concept of **alienation**, with the worker's labour being completely estranged from their essence. It's not a surprise that many creators have expressed burnout on Youtube, as content creation was being rewarded on the basis of factory-like productivity over anything else[^1].

To quote Fisher[^2]; 
> "Over the past thirty years, capitalist realism has successfully installed a 'business ontology' in which it is simply obvious that everything in society, including healthcare and education, should be run as a business."
>
Social media is no exception. And so, our data has become the currency by which tech companies gain power and reinforce their monopolies. 

With our evermore digitalised lives, it should not be the case that the only place where we are guaranteed privacy is in physical human experiences. We should not accept living in a panopticon due to habituation or because we believe we have nothing to hide. Remember, the power that the panopticon holds over us lays not in the fact that it is all-seeing; many times a guard may not even be present in the tower and the prisoners wouldn't even know. The danger comes when it's **mere presence** has the ability to change behaviours. 

There has been a "social amnesia"[^3] or "psychic numbing" that has made many of us forget our right to privacy and therefore freedom in the digital age. But there are changes we can make to regain authority over our digital lives. Although switching from Google or Whatsapp to a FOSS equivalent can come with certain discomfort, it is vital for our freedom in the long-run. Start to question the apparently 'free' software you use, and regain consciousness over the injustices these Big tech companies are getting away with. 

## References 
[^1]:Hernandez, Patricia. “When It Comes to Burnout, YouTube Is Failing Its Creators.” The Verge, The Verge, 21 Sept. 2018, www.theverge.com/2018/9/21/17879652/youtube-creator-youtuber-burnout-problem.
[^2]:Fisher, Mark. Capitalist Realism: Is There No Alternative? Winchester, Zero Books, 27 Nov. 2009.
[^3]:Kavenna, Joanna. “Shoshana Zuboff: “Surveillance Capitalism Is an Assault on Human Autonomy.”” The Guardian, 4 Oct. 2019, www.theguardian.com/books/2019/oct/04/shoshana-zuboff-surveillance-capitalism-assault-human-automomy-digital-privacy.
