---
title: AUC and WhatsApp
author: velthuis_misha
date: 2024-02-28
fediverse: 112343309641825764
description: "A sort summary"
imageAltAttribute: Disfigured Whatsapp logo
tags:
   - Open source
   - Free software
---

Upon arrival at AUC, first year students are invited to join a WhatsApp group of their cohort. Doing so, we feel AUC fails to live up to its responsibility to help students, where possible, find and build a private, safe, (digital) world, devoid of unnecessary commercial interests that might be (or evolve to become) at odds with the interests of the students themselves.

In some parts of the world, including in the Netherlands, WhatsApp has become the most widely used chat service, both for personal and work related purposes. In fact, its wide usage has become one of its main points of attraction; users benefit from the fact that many of their friends, family members and colleagues are on this platform. The more people use Whatsapp, the more attractive it becomes as a chat service.

This effect, also called a [network effect](https://en.wikipedia.org/wiki/Network_effect), has made it increasingly hard to move to another chat service. This is problematic, because there are good reasons for moving away from WhatsApp (written about in more detail in [this](https://opentech_auc.codeberg.page/posts/2024-03-23-whatsapp-is-a-bad-deal/) post).

We believe that the university has a lot of potential (and therefore responsibility) for helping the community find ways out.

1. Firstly, for many students, the university is a place of critical (self-)reflection and new starts: a moment to reconsider which parts of the world that has been given to you are worth carrying along, and which parts you could do without. Given the increasing importance of our digital lives, we believe that this can and should include the main channels of digital communication.

2. Secondly, given that the university brings a lot of people together, and given that it is the place where a lot of new bonds are made that will often last a lifetime, we feel there is no better moment in life, for collectively overcoming some of the above-mentioned lock in that we have inherited from the past.

3. Thirdly, students can be important catalysts for change. Whether it is for their energy, youth, or critical questions, students have historically been able to be powerful points of attraction, as they played important roles in bringing about societal change. Encouraging students to explore different digital worlds, might therefore have an impact beyond the university.

4. Students are still at the beginning of their adult life. When we allow students to get stuck in toxic systems, they will be stuck for a long time. When we help them avoid that, they will be be able to reap the benefits for many years to come.

5. Fourthly, one tried and tested way to overcome collective action problems is to have some central coordination to help the community find a collectively more desirable rallying point. Given that the university takes an active role in bringing new students together in chat groups, we will that this also provides a unique opportunity to help the community find better digital worlds.
