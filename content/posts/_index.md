---
title: "Posts"
date: 2024-03-07
menu: main
weight: 20
---

This section works as a blog for members of the group sharing their takes and
guides on Open Standards, Free Software, and Open Data. You can subscribe
[via RSS](index.xml).
