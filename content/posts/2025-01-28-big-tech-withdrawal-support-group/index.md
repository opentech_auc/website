---
title: "Big Tech Withdrawal Support Workshop"
date: 2025-01-28
author: velthuis_misha
featured:
  url: featured.png
  alt: big tech withdrawal support group
  credit: mfmcl
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
   - Economics
---

As the news is confronting us with the ever increasing political and economic power of the small number of companies and individuals that control our digital lives, we believe the best moment to make the move towards free, open source alternatives is now.

**Wednesday the 29th of January**, from 14:00h to 15:00h OpenTech(AUC) organizes a hybrid workshop to support the community (staff and students) in making (a start with) this transition.

No technical skills required. 

Location: 
- Physical: AUC AB 1.02
- Online: contact us at hello@opentech-auc.org for the link
