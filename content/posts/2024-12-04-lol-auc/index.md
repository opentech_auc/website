---
title: "How a Gossip Account Took the Laugh Out of LOL"
date: 2024-12-04
author: natasha_mr
fediverse: 113697058754950059
featured:
  url: featured.jpg
  alt: A collage of posts from the "AUC lol" account
  caption: Tweets from before @lol.auc.lol's shut-down
  credit: CC-BY-SA-4.0
  previewOnly: false
tags:
   - Free speech
   - Anonimity
   - Social Media
---
   
A social media gossip account named **“lol.auc.lol”** closed on 30 November 2024 after receiving backlash from the AUC community due to offensive posts repeatedly published on the platform. The account permits students to submit **anonymous confessions** by filling out a form on the website, which then gets immediately posted on X and Instagram. However, what started as a silly college gossip account quickly transformed to a platform of hate. Many students were left aghast, questioning its purpose. 

The Instagram account, created in June 2024, had approximately 286 followers and over 670 posts at its peak. The creator, Divyaansh Sethia, a third-year Social Science major at AUC, created this account as an opportunity to learn more about programming. And what better way to gain student engagement than a gossip account? He constructed the website in under two hours – quite a short time for something that would impact the AUC community this much. 

Submitting a confession is extremely easy. Any person can click the link in the Instagram biography of the account, fill in a quick form on the website selecting their major and year from a dropdown menu, write their confession, and press submit. When doing so, the user accepts the **terms and conditions** stated on a link just below the submit button of the website. 

Within these terms and conditions, section 5.4 addresses **prohibited content** such as content that promotes hate speech or discrimination. However, as an anonymous second-year Social Science student highlighted on 9 November 2024 on the confessions page itself, many of the posts violate these terms and conditions.

Indeed, many of the confessions posted on the account could easily be described as offensive and its synonyms. Highlights included a person that denied the Holocaust, offensive and racist statements directed towards Palestinians or Israelis and outright insults to individuals themselves. Even though certain posts were innocent gossip-like confessions, harmless secrets people might feel the urge to spit out, the majority of posts quickly spiralled into a spectrum of **negative, inflammatory statements**. Laura Herberg Fernández, a first-year Social Science major at AUC, said that seeing these posts definitely stained the view she had of the AUC community upon coming here to study. 

The opinions amongst AUC students are multifaceted. Some people believe that the platform should stay up, whilst others think it is a danger to have such a hateful site within a small community. “I think platforms like these can be silly and entertaining, but unregulated and unchecked they can be dangerous to the **mental health** of a community,” says Jared Carter, a second-year Science major at AUC. Carter is an interesting case study for what can happen on such a platform, when looking at the events that followed after a negative comment was made about him. Soon after it was published, people began sending in comments defending him, even offering compliments, until he almost became a celebrity on the platform. Although the comment didn’t affect Carter’s day-to-day life, he acknowledges that “being a topic of public discussion can be very stressful, especially if you’re somebody with social anxiety or just a very private person.” 

It is clear that the content posted on the website can be detrimental to both groups and individuals, and some students wonder why there isn’t any **regulation** on the platform. 

To answer this, we must first understand how the website works. When a confession is submitted, it is automatically sent to the **Twitter API** which posts it in **real-time** without Sethia’s intervention. For Instagram, however, it’s different. He must manually upload the screenshotted X posts as Instagram publications. This usually takes 3 to 5 minutes, and it is “a way to kill time,” as he says. Sethia claims that he mostly does not read the confessions that he shares from X to Instagram, and that he does it “on autopilot.” Some posts are granted the privilege of having a caption added to them, making students question the workings of this automated process.

In response to critical remarks, Sethia refers to section 6 of the website’s conditions of use. This section addresses the liability of the site and the site owner itself, essentially stating that they are not responsible for the content posted on the site and that by using the site the user accepts that they might be **exposed to inappropriate content**.

Sethia strongly believes in **free speech absolutism**. He himself has seen confessions directly insulting him or a group he belongs to, and that has not made him change his mind on having no self-imposed regulations on the platform. He prefers seeing those confessions, because hiding them will do nothing but create a false sense of safety around him. “There’s just the truth and nothing else. This is an exposure to the truth, and any person’s opinion is their truth,” he says. “I’m not saying that’s wrong. I’m not saying that’s right. I’m just saying that it is what it is.”

“Everybody should get a chance to speak, but not everybody should be heard,”  Sethia continues. He believes that the way to combat hate speech is not by limiting people’s voices, but by having better **mechanisms** that regulate these voices. 

However, those regulations are not for a single person to decide. According to him, he is not the one that should be deciding what gets posted or not. Moreover, the account states that users are encouraged to **report** inappropriate content, and the Instagram or X moderation tools have deleted some of the posts. That is why he feels no social responsibility for what the **algorithm** and the **community** together have made public. 

Despite his strong position on freedom of speech, Sethia acknowledges that the platform has become quite a dangerous place; it has evolved into something that is not representative of the **AUC community**, which he holds fond views of. He has thought of ways to implement his coding skills to a project that is more beneficial for the community, such as a gift-giving service, and has closed the gossip account. Roan Jorna, a third-year Science major who is the PR manager of Peer Support, encourages us to use his committee’s initiative of [@aucc0mpliments](https://www.instagram.com/auc0mpliments), where students can also be anonymous and yet make someone smile rather than frown. The account of “lol.auc.lol” has come to an end, but not without raising multiple questions on what the AUC community values in terms of free speech, and whether or not our current society limits *our* truth.
