---
title: "Een Mastodon account voor de UvA"
date: 2025-01-18
author: velthuis_misha
fediverse: 113850859696960803
featured:
  url: featured.png
  alt: Titel van een artikel van de UvA - UvA doet mee aan de Mastodon pilot
  caption: De UvA omarmde in 2023 de Mastodon pilot van Surf, maar maakte tot op heden zelf geen account aan
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
   - UvA
---

*Op 18 januari 2025 stuurden we deze mail naar socialmedia@uva.nl*

Beste lezer,

Wij zijn een groep studenten en medewerkers van Amsterdam University College die zich al [langere tijd](https://opentech-auc.org/) zorgen maakt over de rol van Big Tech en corporate social media in onze samenleving. Wij komen elke week, buiten het curriculum om, bij elkaar om deze zorgen te bespreken, en om alternatieven te onderzoeken en uit te proberen.

De laatste maanden laten wat ons betreft steeds duidelijker zien dat de huidige situatie onhoudbaar is, en dat het tijd wordt om met z'n allen een betere digitale wereld moeten opbouwen.

Uit de interne communicatie maakten we onlangs tot onze opluchting op dat de UvA minder actief wil zijn op Twitter. We hopen dat de UvA nu ook doorpakt, en de juiste vervolgstap maakt.

Vandaag zagen we [deze](https://social.edu.nl/@wlaatje/113849493194996540) oproep van Wladimir Mufty op Mastodon langskomen, en besloten om hier meteen gehoor aan te geven.

Wij zijn ervan overtuigd dat Mastodon de enige optie is die voor de lange termijn de soevereiniteit van gebruikers waarborgt. Daarbij heeft Surf al (zoals Wladimir ook in zijn [LinkedIn](https://www.linkedin.com/posts/wladimirmufty_mastodon-makesocialssocialagain-onderwijs-activity-7286362521651871744-xysD) bericht beschrijft) een eigen server draaien, waarbij de authenticatie heel makkelijk via UvA credentials verloopt.

Tot slot vinden wij dat de universiteit een speciale verantwoordelijkheid heeft om te voorkomen dat elk jaar weer een nieuw cohort wordt meegezogen in een afhankelijkheid waar we om heel duidelijke redenen juist vanaf moeten (zoals we [hier]({{< ref "/posts/2024-02-28-auc-and-whatsapp" >}} "Whatsapp and AUC") ook over Whatsapp betoogden).

Mocht er nog twijfel bestaan over BlueSky en Mastodon:

1. BlueSky is nog steeds een Amerikaans bedrijf dat uiteindelijk investeringen zal moeten terugverdienen met keuzes waar de gebruikers niet of nauwelijks controle over hebben. Een basis op Mastodon (en de Fediverse) is de beste manier om te voorkomen dat we op de lange termijn opnieuw afhankelijk worden van de grillen van een enkele machtige ondernemers.
2. Met een simpele "[bridge](https://openresearch.amsterdam/nl/page/109494/bluesky-en-mastodon-gebruikers-kunnen-nu-met-elkaar-praten-door)" vanuit Mastodon hebben de mensen op BlueSky nog steeds volledig toegang tot een UvA Mastodon account.


Andere organisaties zijn de UvA al voorgegaan, zoals de [Gemeente Amsterdam](https://social.amsterdam.nl/@gemeenteamsterdam), en tal van organisaties binnen de van de [Nederlandse overheid](https://social.overheid.nl/public/local).

Als jullie nog vragen hebben, bijvoorbeeld naar onze ervaringen op Mastodon (en in de Fediverse), horen we dat graag.

Vriendelijke groeten,

OpenTech(AUC)

Blog: https://opentech-auc.org/

Matrix: https://matrix.to/#/#opentech-auc:pub.solar
