---
title: Short Report on our Linux Installation Party
date: 2024-05-23
fediverse: 112491414295516565
featured:
  url: featured.png
  alt: meme saying that you don't just simply install arch linux
  caption: Welcome to Linux
  credit: Lord of the Rings, adapted
  previewOnly: false
tags:
   - Open source
   - Free software
   - Linux

---

In February 2024 around ten students and teachers gathered in StartUpVillage, the most inspiring part of Science Park. Their goal: Learning how to install Arch Linux on a Laptop. As we all know, one does not simply install Arch Linux correctly on the first try. Therefore, help was needed. Breanndán and his colleague Baran - loyal members and friends of OpenTech(AUC) - had the amazing idea of offering their startup's spare ThinkPad laptops as well as Arch Linux bootable USB sticks. That way, everyone could try out several options in the installation menu without the fear of accidentally deleting all their personal data. With the help of lots of pizzas, drinks, and nerdy jokes the perfect atmosphere was created; discussions about people's favorite desktop designs and other deeply philosophical questions were continued during the night. Issues like digital accessibility or data security didn't come to short either. The interdisciplinary background of our group meant that everyone could learn something new. Students inspired by the customization freedom Arch Linux offers took home the ThinkPads to play around with the operating system. A huge shoutout to Breanndán, Baran, and their colleagues for making this event possible!
