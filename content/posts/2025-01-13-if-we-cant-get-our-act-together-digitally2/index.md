---
title: "If we can't get our act together digitally, we won't have our act together anywhere: reason 2"
date: 2025-01-13
author: velthuis_misha
fediverse: 113833402615133379
featured:
  url: featured.jpg
  alt: 3d visualization of Fediverse logo
  caption: 3D visualization of a proposed Fediverse logo. Image via Wikimedia Commons by Eukombos. CC BY-SA 4.0.
  credit: Eukombos
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
   - Economics
---

*This is the second part of a two-part series. See also [this]({{< ref "/posts/2024-11-27-if-we-cant-get-our-act-together-digitally/" >}} "part 1") article.*

---

> "...if we can't convince people to escape the clutches of capitalism when it only requires a USB stick with Linux and a short online course on bash/python, how are we ever going to gain control over (and share the fruits of) other, more material and inert kinds of technology?"

## Who benefits from innovation?

In many ways and in many places, life is better now than it was 100 years ago. Although technological development played a role, it's definitely not the whole story. It was very much also the product of the successful *fight over the benefits* of this technological development.

This becomes apparent when you look at the last few decades, during which we[^1] actually saw considerable technological development, *without* a successful social struggle over the benefits. The result is the infamous [productivity-wage gap](https://www.epi.org/productivity-pay-gap/): while productivity has grown by 80%, real wages (corrected for inflation)[^2] have grown by only 29%.

## You could have had more time 

If the technological development behind the productivity increase would have benefited us all to the same extent, wages would have also risen with 80%. Instead they have only risen with 29%. The rest accumulates with those who do not work for a wage: those who earn their income from *owning* things. In other words: capitalists.

What would have happened if capitalists would have been (forced to be) satisfied with the (already relatively high) income they earned in the late 70s? The way workers actually have been forced to?

If the productivity-wage gap would have been translated into less work for workers, everyone (both workers and capitalists) would be able to earn (consume) the same as they did in 1979, while workers would only have had to work 70% (129/180=0.7) of the time they currently work (e.g. 3.5 instead of 5 days a week, or 8 months per year instead of 11.5). Imagine what you could have done with all that [time](https://mishathings.org/posts/2021-08-23-how-many-people-work-for-me/).

## More servitude, more precarity

Sometimes, it works (for me) to make big abstract arguments small and personal - to *feel more deeply* what has been done to us.

So let's imagine some people in some work place. I imagine that they come up with a way to produce the same in half of the time. They could all take half the week off, and everything else would remain the same.

Instead what happens, is that the capitalist will appropriate the new technology,[^3] and is now able to produce the same with half of the workers, so half of them are fired.

Having cut wage expenses by 50%, with overall production staying the same, the capitalist tells the workers: "I have all this extra stuff. I am happy to share it with you, but what can you do for me in return? Maybe clean my house? Cook my food? Take care of my children? Make a silly little dance that makes me laugh?"

The outcome is that the workers are kept in precarious low paying servitude (forced to find out whatever pleases their former employer), while the capitalist now gets to hire a cleaner, a baby-sitter a gardener and/or a chef. And all of this will be normalized, as if the capitalist is not effectively running the modern equivalent of a 19th century [estate](https://mishathings.org/posts/2021-08-23-how-many-people-work-for-me/) full of butlers and servants.

![](./ayn-rand.png)


Someone might sceptically counter that such a capitalist would be [competing on price](https://mishathings.org/posts/2024-01-12-what-is-capitalism-and-how-to-escape-it/), and can therefore not simply consume the fruits of technological development. It is theoretically possible that the capitalist is forced to cut the price by 50%, or forced to invest all the "surplus production" into making production processes more efficient, in order to keep the business going.

A quick glance at [US inequality trends](https://ourworldindata.org/explorers/inequality?tab=chart&time=1970..latest&country=~USA&Data=World+Inequality+Database+%28Incomes+before+tax%29&Indicator=Gini+coefficient), however, shows us otherwise. Apparently, the pressure to reduce costs is more real when it comes to workers' wages, than it is when it comes to capitalists' income.[^4]

## Sharing the fruits 

It is easy to forget that another world has been possible. In the past, capitalists have been forced to fairly share the fruits of technological development. At least with the workers in their own country. For example, if we return to the [productivity-wage gap](https://www.epi.org/productivity-pay-gap/) we can see how, from 1948 to 1979, wages grew pretty much in line with productivity.

Some of this may have had to do with the [threat of the Soviet Union](http://tankona.free.fr/rasmussenknutsen619.pdf), and the need for capitalists to "sell capitalism" to the workers: to convince them to fight on the capitalist side of the Cold War. 

But in the 1980s, as capitalism emerged triumphant from that Cold War, and Tatcher and Reagan succesfully sold us neoliberalism, the alignment between the two trends broke down.[^5]

## Free and open source software

How does all of this relate to Free and Open Source Software (FOSS)? Is the FOSS movement supposed to bring back the Soviet Union?

Not precisely. Even though the availability (threat?) of Linux is likely to have kept in check the ability of Microsoft and Apple to squeeze its consumers, I do not want to focus on FOSS' ability to make capitalism less bad, but FOSS' ability to lead the way to a wholly different world.

Marxists have been telling us for at least [177 years](https://en.wikipedia.org/wiki/The_Communist_Manifesto): the best way to ensure that the benefits of technological development are fairly shared is to 1. make sure that people own the means to achieve it and 2. prevent individuals from appropriating progress for private gain.[^6]

The way to achieve this, the most revolutionary ones among them would argue, is for workers to simply take over the factories. To have a full-blown revolution.

However much I agree with the basic analysis that capitalism [needs to be replaced with a healthier system of production and consumption](https://mishathings.org/posts/2024-01-12-what-is-capitalism-and-how-to-escape-it/), I find it puzzling that some of the loudest Marxists are the [least interested](https://blog.jwf.io/2019/12/why-foss-is-still-not-on-activist-agendas/) in FOSS.

Because in a way, the world that they are dreaming of is already here, however small and imperfect: every day, millions of people work together to build and maintain the tools that allow them to collectively escape the exploitation of Big Tech. And whenever one of us finds a more elegant, efficient or beautiful way to satisfy some digital need, it is shared with everyone.

The point is: yes, it might not always be easy to spread the message of FOSS. But if we can't convince people to escape the clutches of capitalism when it only requires a USB stick with Linux and a short online course on bash/python, how are we ever going to gain control over (and share the fruits of) other, more material and inert kinds of technology?

Again, as I also concluded the [first part]({{< ref "/posts/2024-11-27-if-we-cant-get-our-act-together-digitally/">}} "part 1") of this series:

"*Seen in this way, the fight for FOSS is simply a test that we need to pass: a first step towards not screwing up the planet.*"

[^1]: Because of data availability (mainly [this graph](https://www.epi.org/productivity-pay-gap/)) this article focuses on the United States.

 [^2]:Inflation is calculated on the basis of the price of certain basic goods, and therefore already includes the cost reductions that are the result of technological development. The text below the [graph](https://www.epi.org/productivity-pay-gap/) has more information on this.

 [^3]:For example because it was developed with the company's physical resources, disregarding the fact that these resources are worth nothing without the labour that is working it.

 [^4]: This failure of the market to force capitalists to limit personal consumption is taken to be one of the signs that maybe we are moving to a new system of production, more akin to feudalism than to capitalism. See [this series of posts](https://mishathings.org/posts/2024-01-12-what-is-capitalism-and-how-to-escape-it/).

 [^5]: The paragraphs below the "productivity-wage gap [graph](https://www.epi.org/productivity-pay-gap/)" have more information about this.

 [^6]:When workers own the work floor, productivity increase can be a reason to work less, produce more, or reduce the size of the team. Such choices could be made in [consensus decision making](https://theanarchistlibrary.org/library/seeds-for-change-consensus-decision-making), and would of course be influenced by the context: are they still facing a competitive global market which forces them to reduce costs? Or has the world already moved beyond that stage?

