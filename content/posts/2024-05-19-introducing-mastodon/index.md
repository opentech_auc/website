---
title: Brief introduction of Mastodon
author: velthuis_misha
date: 2024-05-19
fediverse: 112472596679583694
featured:
  url: featured.png
  alt: Tusky the Mastodon mascot is welcomed to the Mastodon family
  caption: Welcome to Mastodon
  credit: Mastodon
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
---

At OpenTech(AUC) we like to rant about what makes the existing digital worlds we live in terrible. But it's not all bad. Healthier alternatives exist! In this post we would like to introduce the world of Mastodon.

Mastodon is a decentralized, open source, non-corporate alternative to Twitter. It is part of a larger group of apps and platforms that offer a different digital social world, called "The Fediverse", where people can take back control over the spaces where they digitally meet. The Fediverse works without the intervention of polarizing algorithms and egotripping tech entrepreneurs.

Mastodon, like the other platforms of the Fediverse, is ...

- ... decentralized, because there is no one single point of control. In the case of e.g. Twitter, Facebook, Instagram or TikTok users ultimately depend on the whims of the owners of the network, who can at any time decide to change the rules (e.g. what is allowed, what/whose content is promoted/suppressed). In contrast, Mastodon has no central point of control: anyone can start a Mastodon server, invite people to register at that server, and decide on the rules of that server. Next, the people/communities that own/control such a server can decide if they want to connect ("federate") with (users of) other servers. As a user, if you don't like how your own server is run, you can always move to a different one (without losing your connections). 
- ... non-corporate, which means that the network does not exist to run a profit (e.g. by using psychological tricks to expose you to more advertising to make rich people richer), but simply to provide a crucial public good: a digital space to meet and exchange ideas.
- ... open source, free software, which means that anyone can see exactly what is happening "under the hood". Next it is the *community* that builds the software, and if anyone disagrees with the direction the community is taking, it is always possible to make your own copy ("fork") and take the project in your preferred direction.

What is great is that [Surf](https://www.surf.nl/en), the collaborative organisation for IT in Dutch education and research, has set up a Mastodon server for all students and employees at higher education institutes. They also built a little tool that allows you to log in with your normal institutional credentials (that you e.g. use to log in to your university email). Therefore, joining is as easy as clicking [this](https://social.edu.nl) link, and logging in with your institutional credentials.

If you have any questions, or if you run into any problem, we are more than happy to help out. In fact, that is one of the reasons we started our collective: to help each other navigate the discomfort that comes with the exploration of alternative digital worlds.

## Additional material

A lot has already been written about Mastodon, and the Fediverse. Here are some links that we feel make a strong case for this alternative digital world:

- Kurzgesagt about why the current internet is broken, and why something like Mastodon offers hope: [link to add-free YouTube mirror](https://yewtu.be/watch?v=fuFlMtZmvY0), if previous link does not work: [link](https://www.youtube.com/watch?v=fuFlMtZmvY0&t=5).
- The Verge about the Fediverse: [link](https://www.theverge.com/24063290/fediverse-explained-activitypub-social-media-open-protocol).

