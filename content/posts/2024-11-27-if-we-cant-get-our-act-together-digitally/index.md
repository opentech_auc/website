---
title: "If we can't get our act together digitally, we won't have our act together anywhere: reason 1"
date: 2024-11-27
author: velthuis_misha
fediverse: 113561059999419450
featured:
  url: featured.jpg
  alt: 3d visualization of Fediverse logo
  caption: 3D visualization of a proposed Fediverse logo. Image via Wikimedia Commons by Eukombos. CC BY-SA 4.0.
  credit: Eukombos
  previewOnly: false
tags:
   - Open source
   - Free software
   - Politics
   - Economics

---

*This is the first part of a two-part series. See also [this]({{< ref "/posts/2025-01-13-if-we-cant-get-our-act-together-digitally2/">}} "part 2") article.*

---
   
Imagine you're at a house party, and people are getting drunk. Slowly but steadily, they are raising their voice. And as more and more people do, you are going to have to raise your voice as well. At the end of the night, everyone is screaming in each others' ears, without being any more audible than when everyone was talking at a normal volume.

Even if you would want to escape this dynamic, *individually* deciding to lower your voice is not going to amount to anything: your friends will simply be unable to hear what you're saying, and the people around you will fill up the silence. In fact, the only way out, is a *collective* way out: to *collectively* find a way to lower all voices at the same time.

This is a classic ["collective action problem"](https://en.wikipedia.org/wiki/Collective_action_problem): situations in which the pursuit of short-term narrow self-interest (making yourself heard) undermines the pursuit of long-term collective interests (everyone suffering hearing damage), that can only be ameliorated through *collective*, coordinated action.

Such dynamics are everywhere. And they are the main monsters of our time. Whether it's an [arms's race](https://www.theguardian.com/world/2024/nov/14/nuclear-weapons-war-new-arms-race-russia-china-us) between geopolitical rivals, countries using fossil fuel reserves to maintain [international competetiveness](https://www.tni.org/en/nl-a-welfare-state-for-the-fossil-fuel-industry), or companies' spending excessive amounts on [advertising](https://www.investopedia.com/articles/investing/110513/utilizing-prisoners-dilemma-business-and-economy.asp#toc-payoff-matrix) to boost [conspicuous consumption](https://en.wikipedia.org/wiki/Conspicuous_consumption) of [positional goods](https://en.wikipedia.org/wiki/Positional_good): individually, there is no escape. The only way to quit is to *collectively* decide to do so.

Our dependence on a chat app like WhatsApp, or a social media platform like Instagram is also a collective action problem. You are on it, because other people are on it. And this applies to everyone. The only way to escape, is to *collectively* decide to escape: to leave it behind for [something better](https://opentech-auc.org/posts/2024-03-23-whatsapp-is-a-bad-deal/). Something that does not actively try to keep us locked-in. 

Of course, it will be difficult to convince people to leave Whatsapp, and of course, the alternative will not immediately work as well (nor as expected).

But all of this also applies to the other, bigger collective action problems we are facing. In fact, the challenges to collectively coordinate an escape from WhatsApp and Instagram *pale* in comparison to the challenges of collectively coordinating our escape from consumerism, imperialism and capitalism.

To put it in other words: if we can't manage to escape toxic collective lock-in when it is just means convincing people to download a new app, how are we ever going to escape toxic collective lock-in when it means changing the very nature of our everyday life?

Seen in this way, the fight for FOSS is simply a test that we need to pass: a [first step](https://opentech-auc.org/posts/2024-02-28-auc-and-whatsapp/) towards not screwing up the planet.


