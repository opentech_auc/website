---
title: OpenTech(AUC) weekly meeting
description: 
from: 2024-12-04T15:30:00
to: 2024-12-04T16:30:00
recurrence: FREQ=WEEKLY;BYMONTH=1,2,3,4,5,6,9,10,11,12
location: AUC common room, Science Park 113, 1098 XG, Amsterdam
---

Our regular meetings are informal get-togethers. We chat and help each other
with tech-related problems!
