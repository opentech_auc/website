---
title: "Resources"
date: 2025-01-22
menu: main
weight: 30
---

---

# Getting started

The challenge to transition to free and open source software (FLOSS) can easily
feel quite overwhelming: where do I start? Do I have the necessary technical
skills?

At OpenTech(AUC) we believe that there are many ways to move away from Big Tech,
and that everyone, regardless of their skill, can find their own entry-level
project.

On this page, we present a "menu" of "steps to towards more freedom", from which
everyone can pick their own projects, ranging from "free your browser"
(relatively low skill) to "change your operating system" (relatively high
skill).

We strongly believe that the best way to explore free digital alternatives, is
to do so together. So please do get in touch with us, either in our weekly
get-together in the AB, or in our Matrix Space. We are happy to help.

<!-- ##  Free your browser (skill 1/5)

### Why a problem?

By far, most of the people use [Chrome](https://gs.statcounter.com/browser-market-share)
as their daily browser. Since a browser is basically the "window to the
internet" for most people, this gives Google a lot of control over what the
internet is.

Given its market share, Google can require web developers (people who develop
websites), to implement certain changes that work particularly well with its own
browser. Until at some point, the internet becomes impossible to navigate with
other browsers.

Google is also trying to make sure ad blockers do not work anymore on its own
web browser.

## Change your browser

Alternatives include Firefox and Librewolf, where you will always be able to
install an ad blocker, and whatever extensions.

## Change your email provider (skill 1/5)

### Why a problem?

Your email provider can read your emails.

### What can you do?

## Move away from Google Drive, Docs, Sheets (skill 2/5)

### Why a problem?

### What can you do?

- Cryptpad
- Nextcloud

## Install a FOSS operating system

### Why a problem?

### What can you do?

## Move away from Spotify (skill 4/5)

### Why a problem?

### What can you do?

## Become a keyboard warrior (skill 4/5)

### Why a problem?

### What can you do? -->

## Terminology 

| Terminology     | Definition                                                                  | Examples                                  |
|-----------------|-----------------------------------------------------------------------------| ----------------------------------------- |
| Software        | Piece of code able to be run on a computer                                  | web broswer, code editor, PDF reader      |
| Process         | Procedure of running a program on a specific computer                       |                                           |
| Protocol        | Common language spoken by applications over a network                       | HTTP, email, SMS, ActivityPub, AT, Matrix |
| Fork            | Your own modified copy of a piece of software                               | ForgeJo is a fork of GiTea                |

## Give me alternatives!

- https://switching.software
- https://prism-break.org
- https://www.privacyguides.org
- https://codeberg.org/teaserbot-labs/delightful
- https://directory.fsf.org/wiki/Main_Page

## I need help

Drop by at one of our meetings! We can help you in person.
