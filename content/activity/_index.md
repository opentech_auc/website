---
title: "Activity"
date: 2024-03-07
menu: main
weight: 40
---

This feed sums up [our activity](https://codeberg.org/opentech_auc): the code we
push, the issue we comment on, our releases, the pull requests we work on, and
so on. You can also subscribe [via RSS](https://codeberg.org/opentech_auc.rss).

{{< codeberg "opentech_auc">}}
