---
title: "Microposts"
date: 2024-03-07
menu: main
weight: 10
---

We will post our shortest status updates [on Mastodon](https://social.edu.nl/@OpenTech_AUC).
You will also be able to see them embedded in this page, you can subscribe [via RSS](https://social.edu.nl/@OpenTech_AUC.rss).

{{< mastodon "social.edu.nl" "opentech_auc" 112066196898240864 20 >}}
